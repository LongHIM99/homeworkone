package com.example.longhwone.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TeacherController {

    @GetMapping("/teacher")
    @ResponseBody
    public String welcomeTeacher(){ return "Hello Teacher Nich";}

    @GetMapping("/")
    public String welcomeHome(){ return "home";}
}
