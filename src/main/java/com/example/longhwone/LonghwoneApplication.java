package com.example.longhwone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LonghwoneApplication {

    public static void main(String[] args) {
        SpringApplication.run(LonghwoneApplication.class, args);
    }

}
