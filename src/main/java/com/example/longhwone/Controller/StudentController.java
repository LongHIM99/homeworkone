package com.example.longhwone.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StudentController {

    @GetMapping("/student")
    @ResponseBody
    public String welcomeStudent(){ return "Welcome to Mr.HIM Soklong";}
}
